package com.mobiledev.rpm.rolladice;

import android.app.Application;

import com.google.android.gms.ads.MobileAds;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.mobiledev.rpm.rolladice.manager.AdmobManager;
import com.mobiledev.rpm.rolladice.manager.AnalyticsManager;
import com.mobiledev.rpm.rolladice.manager.ShakeDeviceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by renz on 13/09/2017.
 */

@Module
public class ApplicationModule {

    Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    public Application providesApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    public ShakeDeviceManager provideShakeDeviceManager() {
        return new ShakeDeviceManager(mApplication);
    }

    @Provides
    @Singleton
    public AdmobManager provideAdmobManager() {
        MobileAds.initialize(mApplication, "ca-app-pub-3743837892071227~7931472630");
        return new AdmobManager();
    }

    @Provides
    @Singleton
    public AnalyticsManager provideAnalyticsManager() {
        return new AnalyticsManager(FirebaseAnalytics.getInstance(mApplication));
    }

}
