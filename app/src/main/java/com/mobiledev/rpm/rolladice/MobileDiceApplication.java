package com.mobiledev.rpm.rolladice;

import android.app.Application;

/**
 * Created by renz on 13/09/2017.
 */

public class MobileDiceApplication extends Application {

    private ApplicationComponent mApplicationComponent;
    public ApplicationComponent getmApplicationComponent() {
        return mApplicationComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initAppComponent();
    }

    protected void initAppComponent() {
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        mApplicationComponent.inject(this);
    }

}
