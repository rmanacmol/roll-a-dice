package com.mobiledev.rpm.rolladice.view;

import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.AdView;
import com.mobiledev.rpm.rolladice.R;

import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends BaseActivity {

    @Bind(R.id.ivDice1) ImageView ivDice1;
    @Bind(R.id.ivDice2) ImageView ivDice2;
    @Bind(R.id.ivDice3) ImageView ivDice3;
    @Bind(R.id.ivDice4) ImageView ivDice4;
    @Bind(R.id.ivDice5) ImageView ivDice5;
    @Bind(R.id.ivDice6) ImageView ivDice6;
    @Bind(R.id.ivDice7) ImageView ivDice7;
    @Bind(R.id.ivDice8) ImageView ivDice8;
    @Bind(R.id.ivDice9) ImageView ivDice9;
    @Bind(R.id.ivDice10) ImageView ivDice10;
    @Bind(R.id.ivDice11) ImageView ivDice11;
    @Bind(R.id.ivDice12) ImageView ivDice12;
    @Bind(R.id.tvTotalDice) TextView tvTotalDice;
    @Bind(R.id.tvEmpty) TextView tvEmpty;
    @Bind(R.id.tvTotal) TextView tvTotal;
    @Bind(R.id.fabRoll) FloatingActionButton fabRoll;
    @Bind(R.id.fabPlus) FloatingActionButton fabPlus;
    @Bind(R.id.fabMinus) FloatingActionButton fabMinus;
    @Bind(R.id.pbloading) ProgressBar plLoading;
    @Bind(R.id.mainLayout) LinearLayout mainLayout;
    @Bind(R.id.adView) AdView adView;

    private int[] diceImageList = new int[]{
            R.drawable.ic_dice_roll,
            R.drawable.ic_dice_1,
            R.drawable.ic_dice_2,
            R.drawable.ic_dice_3,
            R.drawable.ic_dice_4,
            R.drawable.ic_dice_5,
            R.drawable.ic_dice_6
    };

    private int totalCounter = 0;
    private int totalDice = 12;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        analyticsManager.analytics(this);
        adView.loadAd(admobManager.adRequest());
        if(shakeDeviceManager.isDeviceShaken()){startNow();}

    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void onClickRoll(View view) {
        analyticsManager.analyticsLogEvent("EVENT_ROLL_DICE","CLICK_ROLL_DICE", "ROLL_DICE");
        startNow();
    }

    public void onClickMinus(View view) {
        analyticsManager.analyticsLogEvent("EVENT_MINUS_DICE","CLICK_MINUS_DICE", "MINUS_DICE");
        if (totalDice > 1) {
            tvTotal.setVisibility(View.GONE);
            totalDice--;
            tvTotalDice.setText(getString(R.string.stringTotalDice,totalDice));
            changeDiceDisplay(totalDice);
        }
    }

    public void onClickPlus(View view) {
        analyticsManager.analyticsLogEvent("EVENT_PLUS_DICE","CLICK_PLUS_DICE", "PLUS_DICE");
        if (totalDice < 12) {
            tvTotal.setVisibility(View.GONE);
            totalDice++;
            tvTotalDice.setText(getString(R.string.stringTotalDice,totalDice));
            changeDiceDisplay(totalDice);
        }
    }

    private void startNow() {
        makeSoundVibrate();
        mainLayout.setVisibility(View.GONE);
        plLoading.setVisibility(View.VISIBLE);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                shakeDice();
            }
        }, 1000);
    }

    private void shakeDice() {
        tvTotal.setVisibility(View.VISIBLE);
        tvEmpty.setVisibility(View.GONE);
        mainLayout.setVisibility(View.VISIBLE);
        plLoading.setVisibility(View.GONE);
        fabMinus.setVisibility(View.VISIBLE);
        fabPlus.setVisibility(View.VISIBLE);

        Random randomDice = new Random();
        int rollDice1 = 0,
                rollDice2 = 0,
                rollDice3 = 0,
                rollDice4 = 0,
                rollDice5 = 0,
                rollDice6 = 0,
                rollDice7 = 0,
                rollDice8 = 0,
                rollDice9 = 0,
                rollDice10 = 0,
                rollDice11 = 0,
                rollDice12 = 0;

        if (ivDice1.getVisibility() == View.VISIBLE) {
            rollDice1 = randomDice.nextInt(6) + 1;
            changeDice1(diceImageList[rollDice1]);
        }
        if (ivDice2.getVisibility() == View.VISIBLE) {
            rollDice2 = randomDice.nextInt(6) + 1;
            changeDice2(diceImageList[rollDice2]);
        }
        if (ivDice3.getVisibility() == View.VISIBLE) {
            rollDice3 = randomDice.nextInt(6) + 1;
            changeDice3(diceImageList[rollDice3]);
        }
        if (ivDice4.getVisibility() == View.VISIBLE) {
            rollDice4 = randomDice.nextInt(6) + 1;
            changeDice4(diceImageList[rollDice4]);
        }
        if (ivDice5.getVisibility() == View.VISIBLE) {
            rollDice5 = randomDice.nextInt(6) + 1;
            changeDice5(diceImageList[rollDice5]);
        }
        if (ivDice6.getVisibility() == View.VISIBLE) {
            rollDice6 = randomDice.nextInt(6) + 1;
            changeDice6(diceImageList[rollDice6]);
        }
        if (ivDice7.getVisibility() == View.VISIBLE) {
            rollDice7 = randomDice.nextInt(6) + 1;
            changeDice7(diceImageList[rollDice7]);
        }
        if (ivDice8.getVisibility() == View.VISIBLE) {
            rollDice8 = randomDice.nextInt(6) + 1;
            changeDice8(diceImageList[rollDice8]);
        }
        if (ivDice9.getVisibility() == View.VISIBLE) {
            rollDice9 = randomDice.nextInt(6) + 1;
            changeDice9(diceImageList[rollDice9]);
        }
        if (ivDice10.getVisibility() == View.VISIBLE) {
            rollDice10 = randomDice.nextInt(6) + 1;
            changeDice10(diceImageList[rollDice10]);
        }
        if (ivDice11.getVisibility() == View.VISIBLE) {
            rollDice11 = randomDice.nextInt(6) + 1;
            changeDice11(diceImageList[rollDice11]);
        }
        if (ivDice12.getVisibility() == View.VISIBLE) {
            rollDice12 = randomDice.nextInt(6) + 1;
            changeDice12(diceImageList[rollDice12]);
        }

        totalCounter =
                rollDice1 +
                        rollDice2 +
                        rollDice3 +
                        rollDice4 +
                        rollDice5 +
                        rollDice6 +
                        rollDice7 +
                        rollDice8 +
                        rollDice9 +
                        rollDice10 +
                        rollDice11 +
                        rollDice12;

        tvTotal.setText(getString(R.string.stringTotalCounter,totalCounter));
        tvTotalDice.setText(getString(R.string.stringTotalDice,totalDice));
        speakOut();
    }

    private void changeDiceDisplay(int totalDice) {

        switch (totalDice) {
            case 1:
                updateDiceDisplay(0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0);
                break;
            case 2:
                updateDiceDisplay(0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0);
                break;
            case 3:
                updateDiceDisplay(0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0);
                break;
            case 4:
                updateDiceDisplay(0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0);
                break;
            case 5:
                updateDiceDisplay(1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0);
                break;
            case 6:
                updateDiceDisplay(1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0);
                break;
            case 7:
                updateDiceDisplay(1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0);
                break;
            case 8:
                updateDiceDisplay(1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0);
                break;
            case 9:
                updateDiceDisplay(1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0);
                break;
            case 10:
                updateDiceDisplay(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0);
                break;
            case 11:
                updateDiceDisplay(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0);
                break;
            case 12:
                updateDiceDisplay(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
                break;
            default:
                break;
        }
    }

    private void updateDiceDisplay(
            int d1,
            int d2,
            int d3,
            int d4,
            int d5,
            int d6,
            int d7,
            int d8,
            int d9,
            int d10,
            int d11,
            int d12) {

        if (d1 == 1) {
            ivDice1.setVisibility(View.VISIBLE);
        } else {
            ivDice1.setVisibility(View.INVISIBLE);
        }

        if (d2 == 1) {
            ivDice2.setVisibility(View.VISIBLE);
        } else {
            ivDice2.setVisibility(View.INVISIBLE);
        }

        if (d3 == 1) {
            ivDice3.setVisibility(View.VISIBLE);
        } else {
            ivDice3.setVisibility(View.INVISIBLE);
        }

        if (d4 == 1) {
            ivDice4.setVisibility(View.VISIBLE);
        } else {
            ivDice4.setVisibility(View.INVISIBLE);
        }

        if (d5 == 1) {
            ivDice5.setVisibility(View.VISIBLE);
        } else {
            ivDice5.setVisibility(View.INVISIBLE);
        }

        if (d6 == 1) {
            ivDice6.setVisibility(View.VISIBLE);
        } else {
            ivDice6.setVisibility(View.INVISIBLE);
        }

        if (d7 == 1) {
            ivDice7.setVisibility(View.VISIBLE);
        } else {
            ivDice7.setVisibility(View.INVISIBLE);
        }

        if (d8 == 1) {
            ivDice8.setVisibility(View.VISIBLE);
        } else {
            ivDice8.setVisibility(View.INVISIBLE);
        }

        if (d9 == 1) {
            ivDice9.setVisibility(View.VISIBLE);
        } else {
            ivDice9.setVisibility(View.INVISIBLE);
        }

        if (d10 == 1) {
            ivDice10.setVisibility(View.VISIBLE);
        } else {
            ivDice10.setVisibility(View.INVISIBLE);
        }

        if (d11 == 1) {
            ivDice11.setVisibility(View.VISIBLE);
        } else {
            ivDice11.setVisibility(View.INVISIBLE);
        }

        if (d12 == 1) {
            ivDice12.setVisibility(View.VISIBLE);
        } else {
            ivDice12.setVisibility(View.INVISIBLE);
        }
    }

    private void changeDice1(int drawable) {
        try {
            Glide.with(MainActivity.this)
                    .load(drawable)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(ivDice1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changeDice2(int drawable) {
        try {
            Glide.with(MainActivity.this)
                    .load(drawable)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(ivDice2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changeDice3(int drawable) {
        try {
            Glide.with(MainActivity.this)
                    .load(drawable)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(ivDice3);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changeDice4(int drawable) {
        try {
            Glide.with(MainActivity.this)
                    .load(drawable)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(ivDice4);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changeDice5(int drawable) {
        try {
            Glide.with(MainActivity.this)
                    .load(drawable)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(ivDice5);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changeDice6(int drawable) {
        try {
            Glide.with(MainActivity.this)
                    .load(drawable)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(ivDice6);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changeDice7(int drawable) {
        try {
            Glide.with(MainActivity.this)
                    .load(drawable)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(ivDice7);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changeDice8(int drawable) {
        try {
            Glide.with(MainActivity.this)
                    .load(drawable)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(ivDice8);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changeDice9(int drawable) {
        try {
            Glide.with(MainActivity.this)
                    .load(drawable)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(ivDice9);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changeDice10(int drawable) {
        try {
            Glide.with(MainActivity.this)
                    .load(drawable)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(ivDice10);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changeDice11(int drawable) {
        try {
            Glide.with(MainActivity.this)
                    .load(drawable)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(ivDice11);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changeDice12(int drawable) {
        try {
            Glide.with(MainActivity.this)
                    .load(drawable)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(ivDice12);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        shakeDeviceManager.registerListener();
        super.onResume();
      if (adView != null) {
        adView.resume();
      }
    }

    @Override
    public void onPause() {
        shakeDeviceManager.unRegisterListener();
      if (adView != null) {
        adView.pause();
      }
      super.onPause();
    }

    @Override
    public void onDestroy() {
      shutDownTTS();
      if (adView != null) {
        adView.destroy();
      }
        super.onDestroy();
    }

    //handles crash glide shit
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        shakeDice();
    }

    public void speakOut() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ttsGreater21("" + totalCounter);
        } else {
            ttsUnder20("" + totalCounter);
        }
    }

}
