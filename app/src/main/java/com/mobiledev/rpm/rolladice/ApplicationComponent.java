package com.mobiledev.rpm.rolladice;

import com.mobiledev.rpm.rolladice.view.BaseActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by renz on 13/09/2017.
 */

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    void inject(MobileDiceApplication application);

    void inject(BaseActivity activity);

}


