package com.mobiledev.rpm.rolladice.view;

import android.annotation.TargetApi;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.mobiledev.rpm.rolladice.MobileDiceApplication;
import com.mobiledev.rpm.rolladice.R;
import com.mobiledev.rpm.rolladice.manager.AdmobManager;
import com.mobiledev.rpm.rolladice.manager.AnalyticsManager;
import com.mobiledev.rpm.rolladice.manager.ShakeDeviceManager;

import java.util.HashMap;
import java.util.Locale;

import javax.inject.Inject;

/**
 * Created by renz on 13/09/2017.
 */

public class BaseActivity extends AppCompatActivity implements TextToSpeech.OnInitListener {

    @Inject ShakeDeviceManager shakeDeviceManager;
    @Inject AdmobManager admobManager;
    @Inject AnalyticsManager analyticsManager;

    private TextToSpeech tts;
    private Vibrator vibe;
    private MediaPlayer mp;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mp = MediaPlayer.create(this, R.raw.shake_dice);
        vibe = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        tts = new TextToSpeech(this, this);

        ((MobileDiceApplication) getApplication()).getmApplicationComponent().inject(this);

    }

    public void makeSoundVibrate() {
        mp.start();
        vibe.vibrate(100);
    }

    public void shutDownTTS() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            int result = tts.setLanguage(Locale.US);
            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            }
        } else {
            Log.e("TTS", "Initilization Failed!");
        }
    }

    @SuppressWarnings("deprecation")
    public void ttsUnder20(String text) {
        HashMap<String, String> map = new HashMap<>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, map);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void ttsGreater21(String text) {
        String utteranceId = this.hashCode() + "";
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
    }
}
