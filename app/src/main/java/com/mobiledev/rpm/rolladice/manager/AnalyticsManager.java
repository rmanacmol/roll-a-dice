package com.mobiledev.rpm.rolladice.manager;

import android.app.Activity;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.mobiledev.rpm.rolladice.view.MainActivity;

/**
 * Created by renz on 13/09/2017.
 */

public class AnalyticsManager {

    private FirebaseAnalytics mFirebaseAnalytics;

    public AnalyticsManager(FirebaseAnalytics mFirebaseAnalytics) {
        this.mFirebaseAnalytics = mFirebaseAnalytics;
    }

    public void analytics(Activity activity) {
        mFirebaseAnalytics.setCurrentScreen(activity, "SIMPLE NAME: " + MainActivity.class.getSimpleName(), "MAIN_ACTIVITY");
    }

    public void analyticsLogEvent(String event, String id, String name) {
        Bundle bundle = new Bundle();
        bundle.putString("ITEM_ID", id);
        bundle.putString("ITEM_NAME", name);
        bundle.putString("TYPE", "BUTTON");
        mFirebaseAnalytics.logEvent(event, bundle);
    }


}
